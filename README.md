# Alar UserService

Сервис пользователей. 

Позволяет создавать новых пользователей, 
получать jwt-токены для существующих и 
проверять валидность токенов пользователей в других сервисах.

### Зависимости
* python 3.10+
* poetry

### Установка зависимостей
```shell
poetry install
```

### Запуск сервиса
##### локально в виртуальном окружении:
```shell
uvicorn app.main:app --host "0.0.0.0" --port "3000"
```
##### в docker-compose:
```shell
docker-compose -f docker-compose.yml up
```

### Миграции

###### Создание миграции
```shell
alembic revision --autogenerate -m "message"
```

###### Применение миграции
```shell
alembic upgrade head
```

###### Откат миграции
```shell
alembic downgread <hash_migrate>
```

### Переменные окружения
* `DATABASE_URL` - строка подключения к базе данных
* `SECRET` - секрет для хеширования паролей
* `ACCESS_TOKEN_EXPIRE_MINUTES` - срок действия токена в минутах (по-умолчанию 60)

## API

#### Создание пользователя
###### Request
```json
POST http://localhost:3001/api/v1/user
{
  "login": "string",
  "password": "string"
}
```
###### Response
```json
{
  "login": "string",
  "id": 1
}
```

#### Получение списка пользователей
###### Request
```json
GET http://localhost:3001/api/v1/user
```
###### Response
```json
[
  {
  "login": "string",
  "id": 1
  },
  ...
]
```

#### Получение пользователя по ID
###### Request
```json
POST http://localhost:3001/api/v1/user/<user_id:int>
```
###### Response
```json
{
  "login": "string",
  "id": 1
}
```

#### Аутентификация пользователя и получение токена
###### Request
```json
POST http://localhost:3001/api/v1/user/authenticate
{
  "login": "string",
  "password": "string"
}
```
###### Response
```json
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6Ik5hbWUiLCJleHAiOjE2NDY4NDA1MDR9.eXg9DcX2CMFfFNj8Ro51XtejoEZ5K-0mVhQhYRMdUKQ",
  "token_type": "Bearer"
}
```

#### Валидация токена
###### Request
```json
POST http://localhost:3001/api/v1/user/validate
{
  "access_token": "string"
}
```
###### Response
```json
{
  "valid": true,
  "user": {
    "login": "string",
    "id": 0
  }
}
```

