from fastapi import APIRouter

from . import user

api_router = APIRouter(
    responses={
        404: {"description": "Page not found"},
    },
)
api_router.include_router(user.api_router, prefix='/user')
