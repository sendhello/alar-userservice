import logging
from datetime import timedelta

import bcrypt
from fastapi import APIRouter, HTTPException, status

from app.config import ACCESS_TOKEN_EXPIRE_MINUTES
from app.db import database
from app.db.queries import create_user_query, get_user_by_login_query, get_user_query, get_users_query
from app.db.utils import check_username_password, create_access_token, get_db_user_from_token
from app.schemas.token import Token, TokenValidate
from app.schemas.user import User, UserAuthenticate, UserBase
from app.schemas.validation import ValidateResponse

logger = logging.getLogger(__name__)
api_router = APIRouter()


@api_router.post("/", response_model=User)
async def create_user(user: UserAuthenticate):
    db_user = await database.fetch_one(
        query=get_user_by_login_query(login=user.login),
    )
    if db_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"User with login `{user.login}` already registered",
        )

    hashed_password = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
    user_create = {**UserBase.parse_obj(user).dict(), 'hashed_password': hashed_password.decode('utf8')}
    user_id = await database.execute(query=create_user_query(), values=user_create)
    return await database.fetch_one(
        query=get_user_query(user_id=user_id),
    )


@api_router.get("/", response_model=list[User])
async def read_users():
    db_users = await database.fetch_all(
        query=get_users_query(),
    )
    return db_users


@api_router.get("/{user_id}", response_model=User)
async def read_user(user_id: int):
    db_user = await database.fetch_one(
        query=get_user_query(user_id=user_id),
    )
    if not db_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='User not found')

    return db_user


@api_router.post("/authenticate", response_model=Token)
async def authenticate_user(user: UserAuthenticate):
    db_user = await database.fetch_one(
        query=get_user_by_login_query(login=user.login),
    )
    if db_user is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User not found",
        )

    else:
        is_password_correct = await check_username_password(user)
        if is_password_correct is False:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Password is not correct",
            )

        else:
            access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
            access_token = create_access_token(
                data={"login": user.login},
                expires_delta=access_token_expires,
            )
            return {
                "access_token": access_token,
                "token_type": "Bearer",
            }


@api_router.post("/validate", response_model=ValidateResponse)
async def validate_token(token: TokenValidate):
    """Валидация токена пользователя.
    """
    db_user = await get_db_user_from_token(token.access_token)

    return {
        'valid': db_user is not None,
        'user': db_user,
    }
