from starlette.config import Config

config = Config('.env')

DATABASE_URL: str = config('DATABASE_URL')

SECRET: str = config('SECRET', default='09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7')
ACCESS_TOKEN_EXPIRE_MINUTES: int = config('ACCESS_TOKEN_EXPIRE_MINUTES', default=60)
