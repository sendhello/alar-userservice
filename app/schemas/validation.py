from typing import Optional

from pydantic import BaseModel, Field

from app.schemas.user import User


class ValidateResponse(BaseModel):
    """Ответ валидации.
    """
    valid: bool = Field(title='Статус валидности токена')
    user: Optional[User] = Field(title='Пользователь')
