from pydantic import BaseModel, Field


class UserBase(BaseModel):
    login: str = Field(title='Имя пользователя')


class UserAuthenticate(UserBase):
    password: str = Field(title='Пароль')


class User(UserBase):
    id: int = Field(title='ID пользователя')

    class Config:
        orm_mode = True
