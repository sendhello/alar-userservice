from pydantic import BaseModel, Field


class TokenBase(BaseModel):
    access_token: str = Field(title='Токен')


class Token(TokenBase):
    token_type: str = Field(title='Тип токена')


class TokenValidate(TokenBase):
    pass
