from datetime import datetime, timedelta
from typing import Optional

import bcrypt
import jwt
from jwt.exceptions import DecodeError

from app.config import SECRET
from app.constants import JWT_ALGORITHM
from app.db import database
from app.db.models import User as UserModel
from app.db.queries import get_user_by_login_query
from app.schemas.user import UserAuthenticate


async def check_username_password(user: UserAuthenticate) -> bool:
    """Проверяет пароль.
    """
    db_user: UserModel = await database.fetch_one(
        query=get_user_by_login_query(login=user.login),
    )
    return bcrypt.checkpw(user.password.encode('utf-8'), db_user.hashed_password.encode('utf-8'))


def create_access_token(*, data: dict, expires_delta: timedelta) -> str:
    """Создание jwt-токена из переданных параметров.
    """
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta

    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET, algorithm=JWT_ALGORITHM)

    return encoded_jwt


async def get_db_user_from_token(token: str) -> Optional[UserModel]:
    """Возвращает юзера после проверки валидности jwt-токена.
    """
    try:
        data = jwt.decode(jwt=token.encode('utf-8'), key=SECRET, algorithms=[JWT_ALGORITHM])
    except DecodeError:
        return None

    if datetime.utcfromtimestamp(data['exp']) < datetime.utcnow():
        return None

    db_user: UserModel = await database.fetch_one(
        query=get_user_by_login_query(login=data['login']),
    )
    if db_user is None:
        return None

    return db_user
