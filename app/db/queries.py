from sqlalchemy import insert, select

from app.db.models import User


def get_user_query(user_id: int):
    return select(User).where(User.id == user_id)


def get_user_by_login_query(login: str):
    return select(User).where(User.login == login)


def get_users_query():
    return select(User)


def create_user_query():
    return insert(User)
